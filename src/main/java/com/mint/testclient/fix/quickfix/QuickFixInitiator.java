package com.mint.testclient.fix.quickfix;


import quickfix.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class QuickFixInitiator {

  private FixApplication application;

  public QuickFixInitiator(String fixConfigFile) throws FileNotFoundException, ConfigError {

    application = new FixApplication();

    Initiator initiator;
    File file = new File(fixConfigFile);
    SessionSettings settings = new SessionSettings(new FileInputStream(file));
    MessageStoreFactory storeFactory = new FileStoreFactory(settings);
    LogFactory logFactory = new FileLogFactory(settings);
    DefaultMessageFactory messageFactory = new DefaultMessageFactory();

    initiator = new SocketInitiator(application, storeFactory, settings, logFactory, messageFactory);
    initiator.start();
    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public FixApplication getFixApplication() {
    return application;
  }
}
