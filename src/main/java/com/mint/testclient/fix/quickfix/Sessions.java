package com.mint.testclient.fix.quickfix;

import quickfix.Session;

import java.util.List;

public class Sessions {
  List<Session> sessions;
  public Sessions(List<Session> sessions) {
    this.sessions = sessions;
  }
  public List<Session> getSessions() {
    return this.sessions;
  }
}
