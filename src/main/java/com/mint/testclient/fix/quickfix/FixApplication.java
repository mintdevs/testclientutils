package com.mint.testclient.fix.quickfix;

import com.mint.testclient.fix.messages.MessageCollector;
import com.mint.testclient.fix.messages.MessageUtil;
import quickfix.*;
import quickfix.field.EncryptMethod;
import quickfix.field.MsgType;
import quickfix.field.Password;
import quickfix.field.Username;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeoutException;

public class FixApplication implements Application {


  private Map<SessionID, ArrayBlockingQueue<Message>> messageMap = new HashMap<>();
  private int messageCapacity = 1000;


  public void onCreate(SessionID sessionId) {
    messageMap.put(sessionId, new ArrayBlockingQueue<>(messageCapacity));
    System.out.println("Attemping to log on " + sessionId);
  }

  @Override
  public void onLogon(SessionID sessionId) {
    System.out.println("Logged on ==>" + sessionId);
  }


  @Override
  public void onLogout(SessionID sessionId) {
    System.out.println("onLogout ==>"+ sessionId);
  }

  @Override
  public void toAdmin(Message message, SessionID sessionId) {
    System.out.println("toAdmin ==>"+ sessionId);

    try {
      if( message.getHeader().getField(new MsgType()).getValue() == MsgType.LOGON){

        if(sessionId.getSenderCompID().equals(SenderCompID.MarketData.senderCompId)) {
          message.setField(new Username("trader5"));
          message.setField(new Password("Crypto123"));
        }else if(sessionId.getSenderCompID().equals(SenderCompID.Order.senderCompId)) {
          message.setField(new Username("FixUser1"));
          message.setField(new Password("Crypto123"));
        }

        message.setField(new EncryptMethod(EncryptMethod.NONE_OTHER));
      }
    } catch (FieldNotFound fieldNotFound) {
      fieldNotFound.printStackTrace();
    }

  }

  @Override
  public void fromAdmin(Message message, SessionID sessionId) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon {
    System.out.println("fromAdmin ==>");
  }

  @Override
  public void toApp(Message message, SessionID sessionId) throws DoNotSend {
    System.out.println("->" + message);
  }

  @Override
  public void fromApp(Message message, SessionID sessionId) {
    messageMap.get(sessionId).add(message);
    System.out.println("<-" + message);
  }

  MessageCollector expectMessageType = new MessageCollector();

  public Message expectMessageType(String msgType, Session session) throws TimeoutException {
    return expectMessageType.collectMessage(messageMap.get(session.getSessionID()), new MsgType(msgType));
  }

  public Message expectMessageType(String msgType, Session session, String fields) throws TimeoutException, MessageUtil.InvalidFixStructure {
    return expectMessageType.collectMessage(messageMap.get(session.getSessionID()), msgType, fields);
  }

  public Message expectMessageType(String msgType, Session session, Field... field) throws TimeoutException, MessageUtil.InvalidFixStructure {
    return expectMessageType.collectMessage(messageMap.get(session.getSessionID()), msgType, field);
  }

  public Set<SessionID> getSessionIds() {
    return messageMap.keySet();
  }

  public Session getSession( SenderCompID senderCompID ) {
    for(SessionID sessionID: messageMap.keySet()){
      if( sessionID.getSenderCompID().equals(senderCompID.getSenderCompId())){
        return  Session.lookupSession(sessionID);
      }
    }

    return null;
  }

  public enum SenderCompID {
    MarketData("quote.MINTMView.11"),
    Order("order.MINTM5");

    private String senderCompId;

    SenderCompID(String senderCompId) {
      this.senderCompId = senderCompId;
    }

    public String getSenderCompId(){
      return senderCompId;
    }

  }
}
