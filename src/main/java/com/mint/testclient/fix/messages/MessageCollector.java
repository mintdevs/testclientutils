package com.mint.testclient.fix.messages;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import quickfix.Field;
import quickfix.FieldNotFound;
import quickfix.Message;
import quickfix.field.MsgType;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class MessageCollector {

  private static final Logger logger = LoggerFactory.getLogger(MessageCollector.class);
  private static long MILLIS_TO_WAIT = 10 * 1000L;
  ExecutorService     executor = Executors.newSingleThreadExecutor();

  public static long getMillisToWait() {
    return MILLIS_TO_WAIT;
  }

  public static void setMillisToWait(long millisToWait) {
    MILLIS_TO_WAIT = millisToWait;
  }

  private static Message waitForMsgType(ArrayBlockingQueue<Message> queue, MsgType msgType) {
    Message message;

      try {
        message = queue.poll(10, TimeUnit.SECONDS);
        if (message != null) {
          if (message.getHeader().getString(35).equals(msgType.getValue())) {
            return message;
          } else {
            return waitForMsgType(queue,msgType);
          }
        }
      } catch (InterruptedException | FieldNotFound e) {
        return waitForMsgType(queue, msgType);
        //System.out.println("WAAAAAAA");
      }
    return null;
    }



  private static Message waitForMsgTypeAndFieldsMatch(ArrayBlockingQueue<Message> queue, MsgType msgType, List<String> fields) throws FieldNotFound {
    long startTime = System.currentTimeMillis();
    while ((System.currentTimeMillis() - startTime ) < 9000) {

      Message message = waitForMsgType(queue, msgType);
      boolean found = true;
      for (String field : fields) {
        String tag = field.split("=")[0];
        String value = field.split("=")[1];
        int i = Integer.valueOf(tag);
        if (!message.getString(i).equals(value))
          found = false;
          break;
      }
      if(found){
        return message;
      }
    }
    return null;
  }

  private static Message waitForMsgTypeAndFieldsMatch(ArrayBlockingQueue<Message> queue, MsgType msgType, Field... fields) throws FieldNotFound {
    long startTime = System.currentTimeMillis();

    while ((System.currentTimeMillis() - startTime ) < 20000) {
      Thread.yield();
      Message message = waitForMsgType(queue, msgType);
      boolean found = true;
      for (Field field : fields) {
        if (!message.getString(field.getField()).equals(field.getObject().toString()))
          found = false;
          break;
      }
      if(found){
        return message;
      }
    }
    return null;
  }


  public Message collectMessage(ArrayBlockingQueue<Message> queue, MsgType msgType) throws TimeoutException {
    return findMessageTask(() -> waitForMsgType(queue, msgType));
  }

  public Message collectMessage(ArrayBlockingQueue<Message> queue, String msgType, String fields) throws MessageUtil.InvalidFixStructure, TimeoutException {
    MessageUtil.validateFixString(fields);
    String[] list = fields.split("\\|");
    return findMessageTask(() -> waitForMsgTypeAndFieldsMatch(queue, new MsgType(msgType), Arrays.asList(list)));
  }

  public Message collectMessage(ArrayBlockingQueue<Message> queue, String msgType, Field... fields) throws MessageUtil.InvalidFixStructure, TimeoutException {
    return findMessageTask(() -> waitForMsgTypeAndFieldsMatch(queue, new MsgType(msgType), fields));
  }

  public Message collectMessage(ArrayBlockingQueue<Message> queue, MsgType msgType, Field... fields) throws TimeoutException {
    return findMessageTask(() -> waitForMsgTypeAndFieldsMatch(queue, msgType, fields));
  }

  private Message findMessageTask(Callable<Message> task) throws TimeoutException {
    executor = Executors.newSingleThreadExecutor();
    final Future<Message> future = executor.submit(task);
    try {
      final Message result = future.get(MILLIS_TO_WAIT, TimeUnit.MILLISECONDS);
      return result;
    } catch (TimeoutException e) {
      logger.error(e.getMessage());
      //while(executor.)
      List<Runnable> r = executor.shutdownNow();
      future.cancel(true /* mayInterruptIfRunning */);
//      return null;
      throw e;
    } catch (InterruptedException | ExecutionException e) {
      logger.debug("task interrupted");
      e.printStackTrace();
    }

    executor.shutdownNow();
    return null;
  }


}
