package com.mint.testclient.fix.messages;

import quickfix.FieldNotFound;
import quickfix.field.*;
import quickfix.fix44.*;
import quickfix.fix50.ExecutionAcknowledgement;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class MakerMessageFactory {

  private static Quote createQuote(String symbol) {
    Quote quote = new Quote();
    quote.set(new QuoteID("" + System.currentTimeMillis()));
    quote.set(new Symbol(symbol));
    quote.set(new TransactTime(LocalDateTime.now(ZoneId.of("UTC"))));
    return quote;
  }

  public static Quote createDoubleSidedQuote(String symbol, double bidPx, double offerPx, double bidSize, double offerSize) {
    Quote quote = createQuote(symbol);
    quote.set(new BidPx(bidPx));
    quote.set(new OfferPx(offerPx));
    quote.set(new BidSize(bidSize));
    quote.set(new OfferSize(offerSize));

    return quote;
  }

  public static Quote createOfferQuote(String symbol, double offerPx, double offerSize) {
    Quote quote = createQuote(symbol);
    quote.set(new OfferPx(offerPx));
    quote.set(new OfferSize(offerSize));
    return quote;
  }

  public static Quote createBidQuote(String symbol, double bidPx, double bidSize) {
    Quote quote = createQuote(symbol);
    quote.set(new BidPx(bidPx));
    quote.set(new BidSize(bidSize));
    return quote;
  }

  public static QuoteCancel createCancelQuoteBySymbol(String symbol) {
    QuoteCancel quoteCancel = new QuoteCancel();
    quoteCancel.set(new QuoteCancelType(QuoteCancelType.CANCEL_FOR_ONE_OR_MORE_SECURITIES));
    quoteCancel.setString(55, symbol);
    return quoteCancel;
  }

  //TODO doesnt work
  public static QuoteCancel createCancelAllQuotes() {
    QuoteCancel quoteCancel = new QuoteCancel();
    quoteCancel.set(new QuoteCancelType(QuoteCancelType.CANCEL_ALL_QUOTES));
    return quoteCancel;
  }



  public static ExecutionAcknowledgement createExecAck(ExecutionReport execReport, double lastPx, double lastQty) throws FieldNotFound {

    ExecutionAcknowledgement executionAcknowledgement = new ExecutionAcknowledgement();
    executionAcknowledgement.getHeader().setString(35, "BN");
    executionAcknowledgement.set(execReport.get(new ExecID()));
    executionAcknowledgement.set(execReport.get(new ClOrdID()));
    executionAcknowledgement.set(execReport.get(new Symbol()));
    executionAcknowledgement.set(execReport.get(new Side()));
    executionAcknowledgement.set(new LastQty(lastQty));
    executionAcknowledgement.set(new LastPx(lastPx));
    executionAcknowledgement.setString(64, String.valueOf(LocalDate.now().plusDays(2)));
    executionAcknowledgement.set(new ExecAckStatus(ExecAckStatus.ACCEPTED));

    return executionAcknowledgement;
  }


  public static MarketDataSnapshotFullRefresh createMDSnapshotFullRefresh(String symbol, double px, double spread, double mdEntrySizeBid, double mdEntrySizeOffer ) {
    MarketDataSnapshotFullRefresh mdSnapshot = new MarketDataSnapshotFullRefresh();
    mdSnapshot.set(new Symbol(symbol));
    mdSnapshot.set(new NoMDEntries(2));
    mdSnapshot.set(new MDReqID("" + System.currentTimeMillis()));


    MarketDataSnapshotFullRefresh.NoMDEntries group = new MarketDataSnapshotFullRefresh.NoMDEntries();
    group.set(new MDEntryType('0'));
    group.set(new MDEntryPx(px));
    group.set(MessageUtil.getBaseCcy(symbol));
    group.set(new MDEntrySize(mdEntrySizeBid));
    group.set(new MDEntryDate(LocalDate.now(ZoneId.of("UTC"))));
    group.set(new QuoteCondition("A"));
    group.set(new QuoteEntryID("" + System.currentTimeMillis()));
    mdSnapshot.addGroup(group);

    group.set(new MDEntryType('1'));
    group.set(new MDEntryPx(px + spread));
    group.set(MessageUtil.getBaseCcy(symbol));
    group.set(new MDEntrySize(mdEntrySizeOffer));
    group.set(new MDEntryDate(LocalDate.now(ZoneId.of("UTC"))));
    group.set(new QuoteCondition("A"));
    group.set(new QuoteEntryID("" + System.currentTimeMillis()));
    mdSnapshot.addGroup(group);

    return mdSnapshot;

  }

  public static MarketDataSnapshotFullRefresh createMDSnapshotFullRefreshIndicative(String symbol, double px, double spread, double mdEntrySizeBid, double mdEntrySizeOffer ) {
    MarketDataSnapshotFullRefresh mdSnapshot = new MarketDataSnapshotFullRefresh();
    mdSnapshot.set(new Symbol(symbol));
    mdSnapshot.set(new NoMDEntries(2));
    mdSnapshot.set(new MDReqID("" + System.currentTimeMillis()));

    MarketDataSnapshotFullRefresh.NoMDEntries group = new MarketDataSnapshotFullRefresh.NoMDEntries();
    group.set(new MDEntryType('0'));
    group.set(new MDEntryPx(px));
    group.set(MessageUtil.getBaseCcy(symbol));
    group.set(new MDEntrySize(mdEntrySizeBid));
    group.set(new MDEntryDate(LocalDate.now(ZoneId.of("UTC"))));
    group.set(new QuoteCondition("I"));
    group.set(new QuoteEntryID("" + System.currentTimeMillis()));
    mdSnapshot.addGroup(group);

    group.set(new MDEntryType('1'));
    group.set(new MDEntryPx(px + spread));
    group.set(MessageUtil.getBaseCcy(symbol));
    group.set(new MDEntrySize(mdEntrySizeOffer));
    group.set(new MDEntryDate(LocalDate.now(ZoneId.of("UTC"))));
    group.set(new QuoteCondition("I"));
    group.set(new QuoteEntryID("" + System.currentTimeMillis()));
    mdSnapshot.addGroup(group);

    return mdSnapshot;

  }

  public static MarketDataIncrementalRefresh createMDIncrementalRefresh (String symbol, double px, double spread, double mdEntrySizeBid, double mdEntrySizeOffer ) {
    MarketDataIncrementalRefresh mdSnapshot = new MarketDataIncrementalRefresh();
    mdSnapshot.set(new NoMDEntries(2));
    mdSnapshot.set(new MDReqID("" + System.currentTimeMillis()));

    MarketDataSnapshotFullRefresh.NoMDEntries group = new MarketDataSnapshotFullRefresh.NoMDEntries();
    group.setString(279,"0"); //MDUpdateAction 0 is new
    group.set(new MDEntryType('0'));
    group.setString(278, "" + System.currentTimeMillis()); //MDEntryID not sure if this will work
    group.setString(55, symbol);
    group.set(new MDEntryPx(px));
    group.set(MessageUtil.getBaseCcy(symbol));
    group.set(new MDEntrySize(mdEntrySizeBid));
    group.setString(282, "TEST1"); //MDEntryOriginator not sure if this will work
    mdSnapshot.addGroup(group);

    group.setString(279,"0"); //MDUpdateAction 0 is new
    group.set(new MDEntryType('1'));
    group.setString(278, "" + System.currentTimeMillis()); //MDEntryID not sure if this will work
    group.setString(55, symbol);
    group.set(new MDEntryPx(px + spread));
    group.set(MessageUtil.getBaseCcy(symbol));
    group.set(new MDEntrySize(mdEntrySizeOffer));
    group.setString(282, "TEST2"); //MDEntryOriginator not sure if this will work
    mdSnapshot.addGroup(group);

    return mdSnapshot;

  }


}