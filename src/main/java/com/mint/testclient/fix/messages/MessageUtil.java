package com.mint.testclient.fix.messages;

import quickfix.field.ClOrdID;
import quickfix.field.Currency;
import quickfix.field.TransactTime;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.regex.Pattern;

public class MessageUtil {

  //checks for structure only - not content
  static Pattern pattern = Pattern.compile("^(([0-9]+=[^|]+)(\\|[0-9]+=[^|]+)+)|[0-9]+=[^|]+");

  public static void validateFixString(String msg) throws InvalidFixStructure {
    if (!pattern.matcher(msg).matches())
      throw new InvalidFixStructure(msg);
  }

  public static Currency getBaseCcy(String symbol) {
    return new Currency(symbol.split("/")[0]);
  }

  public static ClOrdID getClOrdID() {
    return new ClOrdID(String.valueOf(System.nanoTime()));
  }

  public static TransactTime getTransactTime() {
    return new TransactTime(LocalDateTime.now(ZoneId.of("UTC")));
  }

  public static String getEpochTime() {
    return String.valueOf(System.currentTimeMillis());
  }

  public static LocalDateTime getTime() {
    return LocalDateTime.now(ZoneId.of("UTC"));
  }

  public static class InvalidFixStructure extends Exception {
    private InvalidFixStructure(String message) {
      super(message + " is not a valid Fix structure!!! 'number'='String' seperated by '|'");
    }
  }
}
