package com.mint.testclient.fix.messages;

import quickfix.FieldNotFound;
import quickfix.Message;
import quickfix.field.*;
import quickfix.fix44.*;
import quickfix.fix50.ExecutionAcknowledgement;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;


public class TakerMessageFactory {

  public static NewOrderSingle createLimitGTCOrder(String symbol, char side, double price, double orderQty) {
    NewOrderSingle message = new NewOrderSingle();
    message.set(MessageUtil.getClOrdID());
    try {
      System.out.println(message.get(new ClOrdID()).toString());
    } catch (FieldNotFound fieldNotFound) {
      fieldNotFound.printStackTrace();
    }
    message.set(new Side(side));
    message.set(new SettlType(SettlType.REGULAR_FX_SPOT_SETTLEMENT));
    message.set(new TimeInForce(TimeInForce.GOOD_TILL_CANCEL));
    message.set(new OrdType(OrdType.LIMIT));
    message.set(MessageUtil.getTransactTime());
    message.set(new Symbol(symbol));
    message.set(new Price(price));
    message.set(MessageUtil.getBaseCcy(symbol));
    message.set(new OrderQty(orderQty));
    message.setString(21, "1");

    return message;
  }

  public static NewOrderSingle createLimitGTCOrder() {
    return createLimitGTCOrder("BTC/USD", Side.SELL, 1000000, 1);
  }

  public static NewOrderSingle createIOCOrder(String symbol, char side, double price, double orderQty) {
    NewOrderSingle message = new NewOrderSingle();
    message.set(MessageUtil.getClOrdID());
    message.set(new Side(side));
    message.set(new SettlType(SettlType.REGULAR_FX_SPOT_SETTLEMENT));
    message.set(new TimeInForce(TimeInForce.IMMEDIATE_OR_CANCEL));
    message.set(new OrdType('X'));
    message.set(MessageUtil.getTransactTime());
    message.set(new Symbol(symbol));
    message.set(new Price(price));
    message.set(MessageUtil.getBaseCcy(symbol));
    message.set(new OrderQty(orderQty));


    return message;

  }

  public static NewOrderSingle createIOCOrder() {
    return createIOCOrder("BTC/USD", Side.BUY, 1.3, 15000000);
  }

  public static NewOrderSingle createIOCSELLOrderSwitchCurrency() {
    NewOrderSingle message = createIOCOrder("BTC/USD", Side.SELL, 4000, 1);
    message.set(new Currency("USD"));
    return message;
  }

  public static NewOrderSingle createIOCBUYOrderSwitchCurrency() {
    NewOrderSingle message = createIOCOrder("BTC/USD", Side.BUY, 4000, 1);
    message.set(new Currency("CHF"));
    return message;
  }

  public static NewOrderSingle createOrderBadClient() {
    NewOrderSingle message = createLimitGTCOrder();
    message.getHeader().setString(116,"badClient");

    return message;
  }

  public static NewOrderSingle createOrderNoPrice() {
    NewOrderSingle message = createLimitGTCOrder();
    message.removeField(44);
    return message;
  }

  public static NewOrderSingle createOrderDupe() {
    NewOrderSingle message = createLimitGTCOrder();
    message.setString(43, "Y");
    return message;
  }

  public static NewOrderSingle updateOrderDupe(NewOrderSingle order) {
    order.setString(43, "Y");
    return order;
  }

  public static NewOrderSingle createOrderZeroPrice() {
    NewOrderSingle message = createLimitGTCOrder();
    message.set(new Price(0));
    return message;
  }

  public static NewOrderSingle createOrderBelowMinQty() {
    NewOrderSingle message = createLimitGTCOrder();
    message.set(new OrderQty(4000000));
    return message;
  }

  public static NewOrderSingle createOrderExpired() {
    NewOrderSingle message = createLimitGTCOrder();
    message.set(new TimeInForce(TimeInForce.GOOD_TILL_DATE));
    message.set(new ExpireTime(LocalDateTime.now(ZoneId.of("UTC")).plusSeconds(5)));
    return message;
  }

  public static NewOrderSingle createGTDOrder() {
    NewOrderSingle order = TakerMessageFactory.createLimitGTCOrder("ETC/USD", Side.BUY, 1.3, 1);
    order.set(new TimeInForce(TimeInForce.GOOD_TILL_DATE));
    order.set(new ExpireTime(LocalDateTime.now(ZoneId.of("UTC")).plusMinutes(5)));
    return order;
  }

  public static NewOrderSingle createLimitOrderWithDiscretion(double price, double discretion, char side, String symbol) {
    NewOrderSingle message = createLimitGTCOrder(symbol, Side.SELL, 1000000, 1);
    message.set(new Side(side));
    message.set(new Price(price));
    message.setInt(388, 2); //discretionInst
    message.setString(389, String.valueOf(discretion));
    return message;
  }

  public static NewOrderSingle createPeggedWithDiscretion(String symbol, char side, double quantity, double price, double discretion) {
    NewOrderSingle message = createLimitGTCOrder(symbol, side, price, quantity);
    message.set(new OrdType('P'));
    message.removeField(44); //remove price field
    message.setInt(388, 2); //discretionInst
    message.setString(389, String.valueOf(discretion));
    message.setInt(1094,2); //peg price type
    message.setInt(211, 0); //discretion offset value
    return message;
  }


  public static OrderCancelReplaceRequest createCancelReplace(NewOrderSingle originalOrder, double newPrice, double newQuantity) throws FieldNotFound {
    OrderCancelReplaceRequest message = new OrderCancelReplaceRequest();
    ClOrdID clOrdID = new ClOrdID();
    OrigClOrdID origClOrdID = new OrigClOrdID(originalOrder.get(clOrdID).getValue());
    Side side = new Side();
    OrdType ordType = new OrdType();
    Symbol symbol = new Symbol();
    Currency currency = new Currency();
    OrderQty orderQty = new OrderQty(newQuantity);
    Price price = new Price(newPrice);
    SettlType settlType = new SettlType();
    TimeInForce timeInForce = new TimeInForce();
    message.set(MessageUtil.getClOrdID());
    message.set(originalOrder.get(currency));
    message.set(orderQty);
    message.set(originalOrder.get(settlType));
    message.set(origClOrdID);
    message.set(originalOrder.get(side));
    message.set(MessageUtil.getTransactTime());
    message.set(originalOrder.get(ordType));
    message.set(originalOrder.get(timeInForce));
    message.set(originalOrder.get(symbol));
    message.set(price);
    return message;
  }

  public static OrderCancelReplaceRequest createCancelReplaceSwitchSide(NewOrderSingle originalOrder, double newPrice, double newQuantity, char side) throws FieldNotFound {
    OrderCancelReplaceRequest message = createCancelReplace(originalOrder, newPrice, newQuantity);
    message.set(new Side(side));
    return message;
  }

  public static OrderCancelRequest createOrderCancel(NewOrderSingle originalOrder) throws FieldNotFound {
    OrderCancelRequest message = new OrderCancelRequest();
    ClOrdID clOrdID = new ClOrdID();
    Side side = originalOrder.get(new Side());
    Symbol symbol = originalOrder.get(new Symbol());
    OrigClOrdID origClOrdID = new OrigClOrdID(originalOrder.get(clOrdID).getValue());
    message.set(origClOrdID);
    message.set(side);
    message.set(symbol);
    message.set(MessageUtil.getClOrdID());
    message.set(MessageUtil.getTransactTime());
    return message;
  }

  public static Message createMassCancel() {
    Message massCancelRequestType = new Message();
    TransactTime transactTime = MessageUtil.getTransactTime();
    massCancelRequestType.getHeader().setString(35, "q");
    massCancelRequestType.setField(11, MessageUtil.getClOrdID());
    massCancelRequestType.setString(530, "7");
    massCancelRequestType.setField(60, transactTime);

    return massCancelRequestType;
  }


  public static ExecutionAcknowledgement createExecAck(ExecutionReport execReport, double lastPx, double lastQty) throws FieldNotFound {

    ExecutionAcknowledgement executionAcknowledgement = new ExecutionAcknowledgement();
    executionAcknowledgement.getHeader().setString(35, "BN");
    executionAcknowledgement.setString(14, (execReport.getString(14)));
    executionAcknowledgement.set(execReport.get(new OrderID()));
    executionAcknowledgement.set(execReport.get(new ExecID()));
    executionAcknowledgement.set(execReport.get(new ClOrdID()));


    executionAcknowledgement.set(execReport.get(new Symbol()));
    executionAcknowledgement.set(execReport.get(new Side()));
    executionAcknowledgement.setString(15, execReport.get(new Currency()).getValue());

    executionAcknowledgement.set(new LastQty(lastQty));
    executionAcknowledgement.set(new LastPx(lastPx));
    executionAcknowledgement.set(execReport.get(new CumQty()));
    executionAcknowledgement.setString(64, String.valueOf(LocalDate.now().plusDays(2)));
    executionAcknowledgement.set(new ExecAckStatus(ExecAckStatus.ACCEPTED));

    return executionAcknowledgement;
  }

  public static ExecutionAcknowledgement createExecAckUnrelated(ExecutionReport execReport, double lastQty, double lastPx) throws FieldNotFound {
    ExecutionAcknowledgement executionAcknowledgement = createExecAck(execReport, lastQty, lastPx);
    executionAcknowledgement.set(new ExecID("12345"));
    return executionAcknowledgement;
  }

  public static MarketDataRequest createMDRequestPegMidPoint(String symbol) {
    MarketDataRequest marketDataRequest = new MarketDataRequest();

    marketDataRequest.setString(262, "" + System.currentTimeMillis()); //MDReqID
    marketDataRequest.set(new SubscriptionRequestType('1'));
    marketDataRequest.set(new MarketDepth('1')); //top of book
    marketDataRequest.set(new MDUpdateType('0')); //full refresh
    marketDataRequest.set(new NoMDEntryTypes('1'));
    marketDataRequest.set(new NoRelatedSym('1'));



    MarketDataRequest.NoMDEntryTypes group = new MarketDataRequest.NoMDEntryTypes();
    group.setString(55, symbol);
    group.setString(269, "PM"); //MDEntryType peg midpoint
    marketDataRequest.addGroup(group);

    return marketDataRequest;
  }

  public static MarketDataRequest createMDRequestLitPool(String symbol) {
    MarketDataRequest marketDataRequest = new MarketDataRequest();
    marketDataRequest.getHeader().setField( new DeliverToCompID("ALL") );
    marketDataRequest.getHeader().setField( new SenderSubID("MINTMView") );
    marketDataRequest.set(new MDReqID(""+System.currentTimeMillis()));
    marketDataRequest.set(new SubscriptionRequestType('1'));
    marketDataRequest.set(new MarketDepth(0));
    marketDataRequest.set(new NoMDEntryTypes(2));
    marketDataRequest.set(new MDUpdateType(0));


    MarketDataRequest.NoMDEntryTypes group = new MarketDataRequest.NoMDEntryTypes();
    group.set(new MDEntryType('0'));
    marketDataRequest.addGroup(group);
    group.set(new MDEntryType('1'));
    marketDataRequest.addGroup(group);

    MarketDataRequest.NoRelatedSym group2 = new MarketDataRequest.NoRelatedSym();
    group2.set(new Symbol(symbol));
    group2.set(new Product(4));
    marketDataRequest.addGroup(group2);

    return marketDataRequest;
  }


  public static MarketDataRequest createMDRequestLitPool(String symbol, String symbol2) {
    MarketDataRequest marketDataRequest = new MarketDataRequest();
    marketDataRequest.set(new MDReqID(""+System.currentTimeMillis()));
    marketDataRequest.set(new SubscriptionRequestType('1'));
    marketDataRequest.set(new MarketDepth(0));
    marketDataRequest.set(new NoMDEntryTypes(2));
    marketDataRequest.set(new MDUpdateType(0));


    MarketDataRequest.NoMDEntryTypes group = new MarketDataRequest.NoMDEntryTypes();
    group.set(new MDEntryType('0'));
    marketDataRequest.addGroup(group);
    group.set(new MDEntryType('1'));
    marketDataRequest.addGroup(group);

    MarketDataRequest.NoRelatedSym group2 = new MarketDataRequest.NoRelatedSym();
    group2.set(new Symbol(symbol));
    marketDataRequest.addGroup(group2);

    group2.set(new Symbol(symbol2));
    marketDataRequest.addGroup(group2);

    return marketDataRequest;
  }

  public static MarketDataRequest createMDRequestLitPool(String symbol, String symbol2, String symbol3) {
    MarketDataRequest marketDataRequest = new MarketDataRequest();
    marketDataRequest.set(new MDReqID(""+System.currentTimeMillis()));
    marketDataRequest.set(new SubscriptionRequestType('1'));
    marketDataRequest.set(new MarketDepth(0));
    marketDataRequest.set(new NoMDEntryTypes(2));
    marketDataRequest.set(new MDUpdateType(0));


    MarketDataRequest.NoMDEntryTypes group = new MarketDataRequest.NoMDEntryTypes();
    group.set(new MDEntryType('0'));
    marketDataRequest.addGroup(group);
    group.set(new MDEntryType('1'));
    marketDataRequest.addGroup(group);

    MarketDataRequest.NoRelatedSym group2 = new MarketDataRequest.NoRelatedSym();
    group2.set(new Symbol(symbol));
    marketDataRequest.addGroup(group2);

    group2.set(new Symbol(symbol2));
    marketDataRequest.addGroup(group2);

    group2.set(new Symbol(symbol3));
    marketDataRequest.addGroup(group2);

    return marketDataRequest;
  }

  public static MarketDataRequest createMDRequestLitPoolRefData(String symbol, char pegType) {
    MarketDataRequest marketDataRequest = new MarketDataRequest();
    marketDataRequest.set(new MDReqID(""+System.currentTimeMillis()));
    marketDataRequest.set(new SubscriptionRequestType('1'));
    marketDataRequest.set(new MarketDepth(0));
    marketDataRequest.set(new NoMDEntryTypes(2));
    marketDataRequest.set(new MDUpdateType(0));


    MarketDataRequest.NoMDEntryTypes group = new MarketDataRequest.NoMDEntryTypes();
    group.set(new MDEntryType(pegType));
    marketDataRequest.addGroup(group);


    MarketDataRequest.NoRelatedSym group2 = new MarketDataRequest.NoRelatedSym();
    group2.set(new Symbol(symbol));
    marketDataRequest.addGroup(group2);

    return marketDataRequest;
  }

}
