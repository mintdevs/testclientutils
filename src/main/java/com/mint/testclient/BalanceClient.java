package com.mint.testclient;

import com.mint.testclient.rest.RestClient;
import com.mint.testclient.rest.dto.LogOn;

import java.io.IOException;

public class BalanceClient {

    private static final String url_base="https://uat.fxinside.net";

    private static final String uri_logon="/fxi/admin/auth/login";
    private static final String uri_logout="/fxi/admin/auth/logout";

    private static final String uri_getbalance="/fxi/fxiapi/account/v1/getBalance?currency=";
    private static final String uri_getbalances="/fxi/fxiapi/account/v1/getBalances";

    public static void main(String[] args) throws IOException {
        getBanalces();
    }


    public static String getBanalces(){
        RestClient restClient = new RestClient();
        String cookie = null;

        try {
            cookie = restClient.logon(url_base+uri_logon, new LogOn( "MintDevBalanceCheck","Crypto123","MINTM5" ));

           // restClient.get( url_base+uri_getbalances,cookie);

            restClient.get( url_base+uri_getbalance+"ETH",cookie);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if( cookie!=null) {
                //restClient.get(url_base + uri_logout, cookie);
            }
        }

        return null;
    }
}
