package com.mint.testclient;

import com.mint.testclient.fix.messages.TakerMessageFactory;
import com.mint.testclient.fix.quickfix.FixApplication;
import com.mint.testclient.fix.quickfix.QuickFixInitiator;
import quickfix.ConfigError;
import quickfix.Session;
import quickfix.field.Side;
import quickfix.fix44.NewOrderSingle;

import java.io.FileNotFoundException;
import java.io.IOException;

public class OrderClient {
    public static void main(String[] args) throws IOException {
        initFixConnection();
    }

    private static void initFixConnection() throws IOException{
        QuickFixInitiator quickFixInitiator = null;

        try {
            quickFixInitiator = new QuickFixInitiator("src/main/resources/fixSessions_order.cfg");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ConfigError configError) {
            configError.printStackTrace();
        }

        if( quickFixInitiator !=null){
            sendOrder( quickFixInitiator );
        }
    }

    private static void sendOrder(QuickFixInitiator quickFixInitiator) {
        Session ordersession =quickFixInitiator.getFixApplication().getSession(FixApplication.SenderCompID.Order);

        if(ordersession == null){
            System.out.println("ERROR: no order session created!!");
            return;
        }

        int count = 0;
        do{
            if(ordersession.isLoggedOn()) {
                sendOrder(ordersession);
                break;
            }

            System.out.println("waiting to logon ...");
            try {
                Thread.sleep(10*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            count++;
        } while(count<1);

    }

    private static void sendOrder(Session ordersession) {
        System.out.println("sending order ...");
        NewOrderSingle order = TakerMessageFactory.createLimitGTCOrder("ETH/USD", Side.BUY, 166.6, 3);
        ordersession.send(order);

         order = TakerMessageFactory.createLimitGTCOrder("ETH/USD", Side.SELL, 176.88, 2);
         ordersession.send(order);

    }
}
