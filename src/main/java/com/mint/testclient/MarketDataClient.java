package com.mint.testclient;

import com.mint.testclient.fix.messages.TakerMessageFactory;
import com.mint.testclient.fix.quickfix.FixApplication;
import com.mint.testclient.fix.quickfix.QuickFixInitiator;
import quickfix.ConfigError;
import quickfix.Session;
import quickfix.field.Side;
import quickfix.fix44.MarketDataRequest;
import quickfix.fix44.NewOrderSingle;

import java.io.FileNotFoundException;
import java.io.IOException;

public class MarketDataClient {
    public static void main(String[] args) throws IOException{

        initFixConnection();
    }

    private static void initFixConnection() throws IOException{
        QuickFixInitiator quickFixInitiator = null;

        try {
            quickFixInitiator = new QuickFixInitiator("src/main/resources/fixSessions_mkd.cfg");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ConfigError configError) {
            configError.printStackTrace();
        }

        if( quickFixInitiator !=null){
            subscribeMarketData( quickFixInitiator );
        }
    }


    private static void subscribeMarketData(QuickFixInitiator quickFixInitiator) {
        Session mdSession =quickFixInitiator.getFixApplication().getSession(FixApplication.SenderCompID.MarketData);
        if(mdSession == null){
            System.out.println("ERROR: no marketdata session created!!");
            return;
        }

        int count = 0;
        do{
            if(mdSession.isLoggedOn()) {
                subscribe(mdSession);
                break;
            }

            System.out.println("waiting to logon ...");
            try {
                Thread.sleep(10*1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            count++;
        } while(count<1);
    }

    private static void subscribe(Session session) {
        System.out.println("subscribe marketdata ...");
        MarketDataRequest marketDataRequest = TakerMessageFactory.createMDRequestLitPool("ETH/USD");
        session.send(marketDataRequest);
    }
}
