package com.mint.testclient.rest;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mint.testclient.rest.dto.LogOn;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.HttpCookie;
import java.util.ArrayList;
import java.util.List;

public class RestClient {

    private static final int TIMEOUT = 1500;

    private ObjectMapper mapper = new ObjectMapper().disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    private RestTemplate restTemplate;

    public RestClient() {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(TIMEOUT);
        factory.setReadTimeout(TIMEOUT);
        restTemplate = new RestTemplate(factory);
    }

    public String logon(String url, LogOn logon) throws Exception {
        String requestJson = new ObjectMapper().writeValueAsString(logon);
        HttpEntity<String> request = new HttpEntity<>(requestJson, createHeaders(MediaType.APPLICATION_JSON,null));

        try {
            HttpEntity<String> responseEntity = restTemplate.exchange(url,HttpMethod.POST, request, String.class);
            return getCookies(responseEntity.getHeaders());
        } catch (Exception e) {
                e.printStackTrace();
                throw e;
        }
    }


    public String get(String url,String cookie) {
        try {
            HttpEntity<String> request = new HttpEntity<>(createHeaders(MediaType.APPLICATION_FORM_URLENCODED,cookie));
            ResponseEntity<String> stringResponseEntity = restTemplate.exchange(url, HttpMethod.GET,request,String.class);
            String result = stringResponseEntity.getBody();
            System.out.println(String.format("Getting data from ( %s ) response: %s", url, result));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    public String post(String url, Object object,String cookie) throws Exception {
        String requestJson = new ObjectMapper().writeValueAsString(object);
        HttpEntity<String> request = new HttpEntity<>(requestJson, createHeaders(MediaType.APPLICATION_JSON,cookie));

        try {
            ResponseEntity<String> stringResponseEntity = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
            String result = stringResponseEntity.getBody();
            System.out.println(String.format("Posting data to ( %s ) json: %s response: %s", url, requestJson, result));
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;

        }
    }


    private HttpHeaders createHeaders(MediaType mediaType,String cookie) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(mediaType);

        if( cookie!=null){
            headers.add("Cookie",cookie);
        }

        return headers;
    }

    private String getCookies(HttpHeaders headers) {
        final List<String> cooks = headers.get("Set-Cookie");
        List<HttpCookie> cookies = new ArrayList<>();
        if (cooks != null && !cooks.isEmpty()) {
            cooks.stream().map((c) -> HttpCookie.parse(c)).forEachOrdered((cook) -> {
                cook.forEach((a) -> {
                    HttpCookie cookieExists = cookies.stream().filter(x -> a.getName().equals(x.getName())).findAny().orElse(null);
                    if (cookieExists != null) {
                        cookies.remove(cookieExists);
                    }
                    cookies.add(a);
                });
            });
        }

        StringBuilder sb = new StringBuilder();
        for (HttpCookie cookie : cookies) {
            sb.append(cookie.getName()).append("=").append(cookie.getValue()).append(";");
        }
        return sb.toString();

    }

}
