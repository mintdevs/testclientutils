package com.mint.testclient.rest.dto;

public class LogOn {
    String user;
    String pass;
    String org;

    public LogOn(String user, String pass, String org) {
        this.user = user;
        this.pass = pass;
        this.org = org;
    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }

    public String getOrg() {
        return org;
    }
}
