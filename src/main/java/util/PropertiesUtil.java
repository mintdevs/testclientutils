package util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static java.nio.charset.StandardCharsets.UTF_8;

public class PropertiesUtil {

  public static JSONObject getYamlFileAsJson(File file) throws IOException {
    return convertRawYamlToJson(utf8FileToString(file));
  }

  private static String utf8FileToString(File file)
      throws IOException {
    byte[] encoded = Files.readAllBytes(file.toPath());
    return new String(encoded, UTF_8);
  }

  private static JSONObject convertRawYamlToJson(String yaml) throws IOException {
    Object obj = new ObjectMapper(new YAMLFactory()).readValue(yaml, Object.class);
    return new JSONObject(new ObjectMapper().writeValueAsString(obj));
  }

}
